library ieee;
use ieee.std_logic_1164.all;


-- The Not Gate
Entity NOT_GATE is
	port (
		A : in  std_logic;
		B : out std_logic
	);
End Entity Not_Gate;

Architecture struct of NOT_GATE is
Begin
	B <= not A;
End Architecture struct;


-- The And gate for 3 inputs.
library ieee;
use ieee.std_logic_1164.all;

Entity AND3_GATE is
	port (
		A,B,C : in  std_logic;
		D     : out std_logic
	);
End Entity AND3_GATE;

Architecture struct of AND3_GATE is
Begin
	D <= A and B and C;
End Architecture struct;


-- The And gate for 2 inputs.
library ieee;
use ieee.std_logic_1164.all;

Entity AND2_GATE is
	port (
		A, B : in  std_logic;
		C    : out std_logic
	);
End Entity AND2_GATE;

Architecture struct of AND2_GATE is
Begin
	C <= A and B;
End Architecture struct;


-- The Xor Gate
library ieee;
use ieee.std_logic_1164.all;

Entity XOR_GATE is
	port(
		A,B : in  std_logic;
		C   : out std_logic
	);
End Entity XOR_GATE;

Architecture struct of XOR_GATE is
Begin
	C <= A xor B;
End Architecture struct;


-- The main circuit
library ieee;
use ieee.std_logic_1164.all;

Entity AND_XNOR is
	port (
		A,B,C,D,E : in  std_logic;
		I         : out std_logic
	);
End Entity AND_XNOR;

Architecture struct of AND_XNOR is
	-- Define the Not Gate Component.
	Component NOT_GATE
		port (
			A : in  std_logic;
			B : out std_logic
		);
	End Component NOT_GATE;

	-- Define the And gate for 3 inputs Component.
	Component AND3_GATE
		port (
			A,B,C : in  std_logic;
			D     : out std_logic
		);
	End Component AND3_GATE;

	-- Define the And gate for 2 inputs Component.
	Component AND2_GATE
		port (
			A,B : in  std_logic;
			C   : out std_logic
		);
	End Component AND2_GATE;

	-- Define the Xor gate Component 
	Component XOR_GATE
		port (
			A,B : in  std_logic;
			C   : out std_logic
		);
	End Component XOR_GATE;

	-- Define internal signals to pass values
	signal Not_A,Not_E,F,G,N : std_logic;
Begin
		-- Inverts the A and E inputs.
		NotForA : NOT_GATE port map(A, Not_A);
		NotForE : NOT_GATE port map(E, Not_E);

		-- Ands together the the in the two and gates fot the first level.
		AndFor3 : AND3_GATE port map (Not_A,B,C,F);
		AndFor2 : AND2_GATE port map (Not_E,D,G);

		-- Xors The two results.
		XorFromAnd : XOR_GATE port map (F, G, N);

		-- Inverts the end result.
		NotXor : NOT_GATE port map (N, I);
End Architecture struct;


