library ieee;
use ieee.std_logic_1164.all;

Entity AND_XNOR is
	port (
		A,B,C,D,E : in  std_logic;
		I         : out std_logic
	);
End Entity AND_XNOR;

Architecture Functional of AND_XNOR is
Begin
	I <= (not A and B and C) xnor (not E and D);
End Architecture Functional;