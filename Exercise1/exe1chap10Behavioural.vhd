library ieee;
use ieee.std_logic_1164.all;

Entity AND_XNOR is
	port (
		A,B,C,D,E : in  std_logic;
		I         : out std_logic
	);
End Entity AND_XNOR;

Architecture Behavioural of AND_XNOR is
	signal G,F : std_logic;
Begin

	-- Behaviour of the First and gate 
	Process(A,B,C)

	Begin
		if A = '1' then
			F <= '0';
		elsif (B = '0' or C = '0') then
			F <= '0';
		else
			F <= '1';
		End if;
	End Process;

	-- the behaviour of the Second and gate
	Process(D,E)

	Begin
		if E = '1' then
			G <= '0';
		elsif D = '0' then
			G <= '0';
		else
			G <= '1';
		End if;
	End Process;

	-- the behaviour of the XNOR gate
	Process(F,G)

	Begin
		if F = G then
			I <= '1';
		else
			I <= '0';
		End if;

	End Process;

End Architecture Behavioural;

