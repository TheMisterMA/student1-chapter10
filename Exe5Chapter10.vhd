library BITLIB;
use BITLIB.bit_pack.all;

Entity Full_Adder is
	port (
		A,B,Cin : in  bit;
		S, Cout : out bit
	);
End Entity Full_Adder;

type TruthTable is array (0 to 6) of bit_vector(0 to 1);

Architecture With_Truth_Table of Full_Adder is
	constant Full_Adder_TT : TruthTable := ("00", "10", "10", "01", "10", "01", "11");
	signal index           : integer range 0 to 6;
	signal Sum_Carry       : bit_vector(0 to 1);
Begin
	index <= vec2int(A&B&Cin);

	-- Assigning the line where the comb is equal to its index
	Sum_Carry <= Full_Adder_TT(index)

	S <= Sum_Carry(0);
	Cout <= Sum_Carry(1);

End Architecture With_Truth_Table;